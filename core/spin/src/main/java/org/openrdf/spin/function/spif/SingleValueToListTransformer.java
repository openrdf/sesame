/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.collect.Iterators;

class SingleValueToListTransformer<E> implements Function<E, List<? extends E>> {
	private static final SingleValueToListTransformer<?> INSTANCE = new SingleValueToListTransformer<Object>();

	@SuppressWarnings("unchecked")
	static <E> Iterator<List<? extends E>> transform(Iterator<E> iter) {
		return Iterators.transform(iter, (SingleValueToListTransformer<E>)INSTANCE);
	}

	@Override
	public List<? extends E> apply(E v) {
		return Collections.singletonList(v);
	}
}