/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import java.util.List;

import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.evaluation.QueryPreparer;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.function.Function;
import org.openrdf.query.algebra.evaluation.util.Statements;
import org.openrdf.spin.function.AbstractSpinFunction;

import info.aduna.iteration.Iterations;


public class Name extends AbstractSpinFunction implements Function {

	public Name() {
		super(SPIF.NAME_FUNCTION.stringValue());
	}

	@Override
	public Value evaluate(ValueFactory valueFactory, Value... args) throws ValueExprEvaluationException {
		if (args.length != 1) {
			throw new ValueExprEvaluationException(String.format("%s requires 1 argument, got %d", getURI(), args.length));
		}
		if (args[0] instanceof Literal) {
			return valueFactory.createLiteral(((Literal)args[0]).getLabel());
		}
		else {
			QueryPreparer qp = getCurrentQueryPreparer();
			try {
				List<Literal> labels = Iterations.asList(Statements.getObjectLiterals((Resource) args[0], RDFS.LABEL, qp.getTripleSource()));
				return !labels.isEmpty() ? labels.get(0) : valueFactory.createLiteral(args[0].stringValue());
			}
			catch (QueryEvaluationException e) {
				throw new ValueExprEvaluationException(e);
			}
		}
	}
}
