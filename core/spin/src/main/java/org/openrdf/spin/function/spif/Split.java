/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import java.util.Iterator;
import java.util.List;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.spin.function.InverseMagicProperty;

import info.aduna.iteration.CloseableIteration;
import info.aduna.iteration.CloseableIteratorIteration;

public class Split implements InverseMagicProperty {

	@Override
	public String getURI() {
		return SPIF.SPLIT_PROPERTY.toString();
	}

	@Override
	public CloseableIteration<? extends List<? extends Value>, QueryEvaluationException> evaluate(
			final ValueFactory valueFactory, Value... args) throws QueryEvaluationException {
		if (args.length != 2) {
			throw new ValueExprEvaluationException(String.format("%s requires 2 arguments, got %d", getURI(), args.length));
		}
		if (!(args[0] instanceof Literal)) {
			throw new ValueExprEvaluationException("First list element must be a literal");
		}
		if (!(args[1] instanceof Literal)) {
			throw new ValueExprEvaluationException("Second list element must be a literal");
		}
		final String s = ((Literal)args[0]).stringValue();
		final String regex = ((Literal)args[1]).stringValue();
		final String[] parts = s.split(regex);
		return new CloseableIteratorIteration<List<? extends Value>, QueryEvaluationException>(
				SingleValueToListTransformer.transform(new Iterator<Value>()
		{
			int pos = 0;

			@Override
			public boolean hasNext() {
				return (pos < parts.length);
			}

			@Override
			public Value next() {
				return valueFactory.createLiteral(parts[pos++ ]);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}));
	}
}
