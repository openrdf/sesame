/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.datatypes.XMLDatatypeUtil;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.function.BinaryFunction;

public class ParseDate extends BinaryFunction {

	private static final DatatypeFactory datatypeFactory;

	static {
		try {
			datatypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new Error("Could not instantiate javax.xml.datatype.DatatypeFactory", e);
		}
	}

	@Override
	public String getURI() {
		return SPIF.PARSE_DATE_FUNCTION.toString();
	}

	@Override
	protected Value evaluate(ValueFactory valueFactory, Value arg1, Value arg2)
		throws ValueExprEvaluationException
	{
		if (!(arg1 instanceof Literal) || !(arg2 instanceof Literal)) {
			throw new ValueExprEvaluationException("Both arguments must be literals");
		}
		Literal value = (Literal) arg1;
		Literal format = (Literal) arg2;

		FieldAwareGregorianCalendar cal = new FieldAwareGregorianCalendar();

		SimpleDateFormat formatter = new SimpleDateFormat(format.getLabel());
		formatter.setCalendar(cal);
		try {
			formatter.parse(value.getLabel());
		} catch (ParseException e) {
			throw new ValueExprEvaluationException(e);
		}

		XMLGregorianCalendar xmlCal = datatypeFactory.newXMLGregorianCalendar(cal);
		if(!cal.isDateSet()) {
			xmlCal.setYear(DatatypeConstants.FIELD_UNDEFINED);
			xmlCal.setMonth(DatatypeConstants.FIELD_UNDEFINED);
			xmlCal.setDay(DatatypeConstants.FIELD_UNDEFINED);
		}
		if(!cal.isTimeSet()) {
			xmlCal.setHour(DatatypeConstants.FIELD_UNDEFINED);
			xmlCal.setMinute(DatatypeConstants.FIELD_UNDEFINED);
			xmlCal.setSecond(DatatypeConstants.FIELD_UNDEFINED);
		}
		if(!cal.isMillisecondSet()) {
			xmlCal.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
		}

		String dateValue = xmlCal.toXMLFormat();
		QName dateType = xmlCal.getXMLSchemaType();
		if (!cal.isTimezoneSet()) {
			int len = dateValue.length();
			if (dateValue.endsWith("Z")) {
				dateValue = dateValue.substring(0, len-1);
			}
			else if (dateValue.charAt(len-6) == '+' || dateValue.charAt(len-6) == '-') {
				dateValue = dateValue.substring(0, len-6);
			}
		}

		return valueFactory.createLiteral(dateValue, XMLDatatypeUtil.qnameToURI(dateType));
	}

	static final class FieldAwareGregorianCalendar extends GregorianCalendar {
		Set<Integer> fieldsSet = new HashSet<Integer>();

		@Override
		public void set(int field, int value) {
			super.set(field, value);
			fieldsSet.add(field);
		}

		boolean isDateSet() {
			return fieldsSet.contains(Calendar.YEAR)
				|| fieldsSet.contains(Calendar.MONTH)
				|| fieldsSet.contains(Calendar.DAY_OF_MONTH)
				|| fieldsSet.contains(Calendar.DAY_OF_WEEK)
				|| fieldsSet.contains(Calendar.DAY_OF_WEEK_IN_MONTH)
				|| fieldsSet.contains(Calendar.DAY_OF_YEAR);
		}

		boolean isTimeSet() {
			return fieldsSet.contains(Calendar.HOUR_OF_DAY)
				|| fieldsSet.contains(Calendar.HOUR)
				|| fieldsSet.contains(Calendar.MINUTE)
				|| fieldsSet.contains(Calendar.SECOND);
		}

		boolean isMillisecondSet() {
				return fieldsSet.contains(Calendar.MILLISECOND);
		}

		boolean isTimezoneSet() {
			return fieldsSet.contains(Calendar.ZONE_OFFSET);
		}
	}
}
