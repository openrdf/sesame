/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openrdf.model.IRI;
import org.openrdf.model.Value;
import org.openrdf.query.impl.MapBindingSet;
import org.openrdf.query.parser.ParsedBooleanQuery;
import org.openrdf.query.parser.ParsedGraphQuery;
import org.openrdf.query.parser.ParsedOperation;
import org.openrdf.query.parser.ParsedTupleQuery;
import org.openrdf.query.parser.ParsedUpdate;

import com.google.common.base.Joiner;

/**
 * Class to represent a SPIN template.
 */
public class Template {

	private final IRI IRI;

	private ParsedOperation parsedOp;

	private final List<Argument> arguments = new ArrayList<Argument>(4);

	public Template(IRI IRI) {
		this.IRI = IRI;
	}

	public IRI getUri() {
		return IRI;
	}

	public void setParsedOperation(ParsedOperation op) {
		this.parsedOp = op;
	}

	public ParsedOperation getParsedOperation() {
		return parsedOp;
	}

	public void addArgument(Argument arg) {
		arguments.add(arg);
	}

	public List<Argument> getArguments() {
		return arguments;
	}

	public ParsedOperation call(Map<IRI, Value> argValues)
		throws MalformedSpinException
	{
		MapBindingSet args = new MapBindingSet();
		for(Argument arg : arguments) {
			IRI argPred = arg.getPredicate();
			Value argValue = argValues.get(argPred);
			if(argValue == null && !arg.isOptional()) {
				throw new MalformedSpinException("Missing value for template argument: "+argPred);
			}
			if(argValue == null) {
				argValue = arg.getDefaultValue();
			}
			if(argValue != null) {
				args.addBinding(argPred.getLocalName(), argValue);
			}
		}

		ParsedOperation callOp;
		if(parsedOp instanceof ParsedBooleanQuery) {
			callOp = new ParsedBooleanTemplate(this, args);
		}
		else if(parsedOp instanceof ParsedTupleQuery) {
			callOp = new ParsedTupleTemplate(this, args);
		}
		else if(parsedOp instanceof ParsedGraphQuery) {
			callOp = new ParsedGraphTemplate(this, args);
		}
		else if(parsedOp instanceof ParsedUpdate) {
			callOp = new ParsedUpdateTemplate(this, args);
		}
		else {
			throw new AssertionError("Unrecognised ParsedOperation: "+parsedOp.getClass());
		}
		return callOp;
	}

	@Override
	public String toString() {
		return IRI+"("+ Joiner.on(", ").join(arguments)+")";
	}
}
