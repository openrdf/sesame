/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function;

import org.openrdf.query.algebra.evaluation.function.TupleFunction;

/**
 * Magic properties are normally treated as {@link TupleFunction}s
 * acting on the subject of a statement.
 * However, there are many cases where it makes more sense to treat
 * a magic property as acting on the object of a statement instead.
 * Any TupleFunction implementing this interface will be given the object
 * of the magic property as the argument and the result will be bound
 * to the subject of the magic property.
 */
public interface InverseMagicProperty extends TupleFunction {
}
