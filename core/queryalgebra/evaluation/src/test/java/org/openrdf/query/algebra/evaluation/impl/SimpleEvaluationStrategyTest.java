package org.openrdf.query.algebra.evaluation.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import info.aduna.iteration.CloseableIteration;

import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.algebra.evaluation.EvaluationStrategy;
import org.openrdf.query.algebra.evaluation.QueryBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.QueryParserUtil;

public class SimpleEvaluationStrategyTest {

	private EvaluationStrategy strategy;

	@Before
	public void setUp()
		throws Exception
	{
		strategy = new SimpleEvaluationStrategy(new EmptyTripleSource(), null);
	}

	/**
	 * Verifies if only those input bindings that actually occur in the query
	 * are returned in the result. See SES-2373.
	 */
	@Test
	public void testBindings()
		throws Exception
	{
		String query = "SELECT ?a ?b WHERE {}";
		ParsedQuery pq = QueryParserUtil.parseQuery(QueryLanguage.SPARQL, query, null);

		QueryBindingSet constants = new QueryBindingSet();
		constants.addBinding("a", ValueFactoryImpl.getInstance().createLiteral("foo"));
		constants.addBinding("b", ValueFactoryImpl.getInstance().createLiteral("bar"));
		constants.addBinding("x", ValueFactoryImpl.getInstance().createLiteral("X"));
		constants.addBinding("y", ValueFactoryImpl.getInstance().createLiteral("Y"));

		CloseableIteration<BindingSet, QueryEvaluationException> result = strategy.evaluate(pq.getTupleExpr(),
				constants);
		assertNotNull(result);
		assertTrue(result.hasNext());
		BindingSet bs = result.next();
		assertTrue(bs.hasBinding("a"));
		assertTrue(bs.hasBinding("b"));
		assertFalse(bs.hasBinding("x"));
		assertFalse(bs.hasBinding("y"));
	}

}
