/* Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.sail.spin.config;

import org.openrdf.model.IRI;
import org.openrdf.model.impl.SimpleValueFactory;

/**
 * Vocabulary constants for RDF-based configuration of the SpinSail.
 * 
 * @author Jeen Broekstra
 */
public class SpinSailSchema {

	/**
	 * The SpinSail schema namespace (
	 * <tt>http://www.openrdf.org/config/sail/spin#</tt>).
	 */
	public static final String NAMESPACE = "http://www.openrdf.org/config/sail/spin#";

	/**
	 * http://www.openrdf.org/config/sail/spin#axiomClosureNeeded
	 */
	public static final IRI AXIOM_CLOSURE_NEEDED = create("axiomClosureNeeded");

	private static final IRI create(String localName) {
		return SimpleValueFactory.getInstance().createIRI(NAMESPACE, localName);
	}

	private SpinSailSchema() {
	};
}
